# Battleship
You are playing with a computer player. 
Battleships are initialised at random in a 10x10 grid.
There is a carrier (length 5), battleship (length 4),
cruiser (length 3), submarine (length 3) and destroyer (length 2).
Take turns until someone's entire fleet of ship has been sunk.
Enter the coordinates (x,y), where x is horizontal and y vertical.
