package game;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import grid.Sea;
import player.Player;
import player.PlayerAuto;
import player.PlayerUser;
import ship.ShipType;

public class Game {
	public static final List<ShipType> SHIP_INIT_LIST = new ArrayList<>();
	static {
		SHIP_INIT_LIST.add(ShipType.CARRIER);
		SHIP_INIT_LIST.add(ShipType.BATTLESHIP);
		SHIP_INIT_LIST.add(ShipType.CRUISER);
		SHIP_INIT_LIST.add(ShipType.SUBMARINE);
		SHIP_INIT_LIST.add(ShipType.DESTROYER);
	}
	
	int numPlayers;
	List<Player> players = new ArrayList<>();
	
	public Game(int numPlayers) {
		this.numPlayers = numPlayers;
		players.add(new PlayerUser("User"));
		for (int i = 0; i < numPlayers - 1; i++) {
			players.add(new PlayerAuto("Computer player " + i));
		}
		launch();
	}
	
	
	public List<ShipType> getShipInitList() {
		return SHIP_INIT_LIST;
	}
	
	public void launch() {
		int i = -1;
		while(true) {
			i++;
			Player attacker = players.get(i % numPlayers);
			Player defender = players.get((i+1) % numPlayers);
			attacker.askAttack(defender);


			if (!defender.isAlive()) {
				System.out.println(attacker + " killed " + defender + "! ");
				break;
			}
		}
	}
	
}
