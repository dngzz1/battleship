package player;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import game.Game;
import grid.Sea;
import grid.State;
import ship.Orientation;
import ship.Ship;
import ship.ShipType;

public abstract class Player {
	static int totalNum = 0;
	int id;
	String name;
	int score = 0;
	Sea sea = new Sea();
	Sea attackBoard = new Sea();
	List<Ship> ships = new ArrayList<>();
	
	public Player(String name) {
		this.id = totalNum++;
		this.name = name;
		initShips();
	}
	
	abstract void initShips();

	void initRandomShips() {
		for (ShipType shipType : Game.SHIP_INIT_LIST) {
			int length = shipType.getLength();
			while (true) {
				int x = -1;
				int y = -1;
				Orientation orientation = Orientation.getRandom();
				switch(orientation) {
					case HORIZONTAL:
						x = ThreadLocalRandom.current().nextInt(0, Sea.SIZE - length);
						y = ThreadLocalRandom.current().nextInt(0, Sea.SIZE - 1);
						break;
					case VERTICAL:
						x = ThreadLocalRandom.current().nextInt(0, Sea.SIZE - 1);
						y = ThreadLocalRandom.current().nextInt(0, Sea.SIZE - length);
				}
				Ship ship = new Ship(orientation, shipType, x, y);
				if(sea.canAdd(ship)) {
					ships.add(ship);
					sea.add(ship);
					break;
				}
			}
		}
	}
	


	
	public abstract void askAttack(Player player);
	
	public final void attack(Player player, int x, int y) {
		if (player.sea.getState(x,y).isShip()) {
			System.out.println("Kaboom!");
			player.sea.setState(x,y, State.WRECK);
			attackBoard.setState(x,y,State.WRECK);
		} else {
			System.out.println("Missed!");
			player.sea.setState(x,y, State.MISSED);
			attackBoard.setState(x,y,State.MISSED);
		}
	}

	public Sea getAttackBoard() {
		return attackBoard;
	}
	
	public final boolean isAlive() {
		return sea.getAlive() > 0;
	}

	@Override
	public String toString() {
		return name;
	}


	public Sea getSea() {
		return sea;
	}
}
