package player;

import java.util.Scanner;

import game.Game;
import grid.Sea;
import ship.Orientation;
import ship.Ship;
import ship.ShipType;

public class PlayerUser extends Player {
	private static final Scanner scanner = new Scanner(System.in);
	public PlayerUser(String name) {
		super(name);
	}

	void initShips() {
		initAskShips();
	}

	void initAskShips() {
		System.out.println(sea);
		for (ShipType shipType : Game.SHIP_INIT_LIST) {
			int x;
			int y;
			char c;
			Ship ship = null;
			String input;
			while (ship == null) {
				System.out.println("Place a ship of type " + shipType + "(length = " + shipType.getLength()
						+ ") at (x,y,orientation) separated by a comma. \n" + "x and y must be between 0 and "
						+ (Sea.SIZE - 1)
						+ " and orientation is either horizontal (H) or vertical (V), \ne.g. 3,4,H");
				input = scanner.nextLine();
				String[] tmp = input.split(",");
				// check that we have 3 arguments.
				if (tmp.length != 3) {
					System.out.println("You only supplied " + tmp.length + " arguments. Try again.");
					continue;
				}
				// check that x and y are integers.
				try {
					x = Integer.parseInt(tmp[0]);
				} catch (NumberFormatException e) {
					System.out.println("First argument is not valid. Try again.");
					continue;
				}
				try {
					y = Integer.parseInt(tmp[1]);
				} catch (NumberFormatException e) {
					System.out.println("Second argument is not valid. Try again.");
					continue;
				}
				c = tmp[2].charAt(0);
				c = Character.toUpperCase(c);
				switch (c) {
				case 'H':
					ship = new Ship(Orientation.HORIZONTAL, shipType, x, y);
					break;
				case 'V':
					ship = new Ship(Orientation.VERTICAL, shipType, x, y);
					break;
				default:
					System.out.println("Third argument is not valid. Try again.");
					continue;
				}
				// check that the ship is in bound.
				if (!sea.canAdd(ship)) {
					System.out.println("You cannot place ship there. Try again.");
					ship = null;
				}
			}
			ships.add(ship);
			sea.add(ship);
			System.out.println(sea);
		}
	}

	@Override
	public void askAttack(Player player) {
		String input;
		int x;
		int y;
		while (true) {
			System.out.println("Enter a coordinate to bomb, separated by commas");
			System.out.println(attackBoard);
			input = scanner.nextLine();
			String[] tmp = input.split(",");
			// check that we have 2 arguments.
			if (tmp.length != 2) {
				System.out.println("You only supplied " + tmp.length + " arguments. Try again.");
				continue;
			}
			// check that x and y are integers.
			try {
				x = Integer.parseInt(tmp[0]);
			} catch (NumberFormatException e) {
				System.out.println("First argument is not valid. Try again.");
				continue;
			}
			try {
				y = Integer.parseInt(tmp[1]);
			} catch (NumberFormatException e) {
				System.out.println("Second argument is not valid. Try again.");
				continue;
			}
			if (x < 0 || x >= Sea.SIZE) {
				System.out.println("x value is not valid. Try again.");
				continue;
			}
			if (y < 0 || y >= Sea.SIZE) {
				System.out.println("y value is not valid. Try again.");
				continue;
			}
			break;
		}
		attack(player, x, y);
		System.out.println(player.name + "'s board:");
		System.out.println(attackBoard);
		System.out.println("<PRESS ENTER>");
		scanner.nextLine();
	}

}
