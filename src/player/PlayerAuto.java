package player;

import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;

import game.Game;
import grid.Sea;
import ship.Orientation;
import ship.Ship;
import ship.ShipType;

public class PlayerAuto extends Player {

	public PlayerAuto(String name) {
		super(name);
		
	}
	
	 void initShips() {
		initRandomShips();
	}

	@Override
	public void askAttack(Player player) {
		Scanner scanner = new Scanner(System.in);
		int x = ThreadLocalRandom.current().nextInt(0, Sea.SIZE);
		int y = ThreadLocalRandom.current().nextInt(0, Sea.SIZE);
		System.out.println(name + " attempts to bomb (" + x + "," + y + "). <PRESS ENTER>");
		scanner.nextLine();
		attack(player, x, y);
		System.out.println(attackBoard);
		System.out.println("<PRESS ENTER>");
		scanner.nextLine();
		
	}

}
