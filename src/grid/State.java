package grid;

public enum State {
	SEA(" ", false),
	CARRIER("5", true),
	BATTLESHIP("4", true),
	CRUISER("3", true),
	SUBMARINE("3", true),
	DESTROYER("2", true),
	MISSED("X", false),
	WRECK("@", false);
	
	private final String symbol;
	private final boolean isShip;
	State(String symbol, boolean isShip) {
		this.symbol = symbol;
		this.isShip = isShip;
	}
	
	public String getSymbol() {
		return symbol;
	}
	public boolean isShip() {
		return isShip;
	}
}
