package grid;

import java.util.List;

import ship.Ship;

public class Sea {
	
	private final State[][] state;
	public static final int SIZE = 10;
	
	public Sea() {
		this.state = new State[SIZE][SIZE];
		for (int i = 0; i < SIZE; i++) {
			for (int j = 0; j < SIZE; j++) {
				state[i][j] = State.SEA;
			}
		}
	}
	
	public void add(Ship ship) {
		int x = ship.getX();
		int y = ship.getY();
		int length = ship.getLength();
		switch(ship.getOrientation()) {
		case HORIZONTAL:
			for (int i = 0; i < length; i++) {
				state[x+i][y] = ship.getShipType().getState();
			}
			break;
		case VERTICAL:
		default:
			for (int i = 0; i < length; i++) {
				state[x][y+i] = ship.getShipType().getState();
			}
			break;
		}
	}

	
	public boolean canAdd(Ship ship) {
		int x = ship.getX();
		int y = ship.getY();
		int length = ship.getLength();
		switch(ship.getOrientation()) {
		case HORIZONTAL:
			if (x < 0 || x + length - 1 >= SIZE) {
				return false;
			}
			for (int i = 0; i < length; i++) {
				if (state[x + i][y] != State.SEA) {
					return false;
				}
			}
			return true;
			
		case VERTICAL:
			if (y < 0 || y + length - 1 >= SIZE) {
				return false;
			}
			for (int j = 0; j < length; j++) {
				if (state[x][y + j] != State.SEA) {
					return false;
				}
			}
			return true;
		
		default:
			System.out.println("Ship has no orientation");
			return false;
		}
	}
	
	public int getAlive() {
		int count = 0;
		for (int i = 0; i < SIZE; i++) {
			for (int j = 0; j < SIZE; j++) {
				if(state[i][j].isShip()) {
					count++;
				}
			}
		}
		return count;
	}
	
	
	public State getState(int x, int y) {
		return state[x][y];
	}

	public void setState(int x, int y, State s) {
		state[x][y] = s;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(" ");
		for (int i = 0; i < SIZE; i++) {
			sb.append(i);
			sb.append(" ");
		}
		sb.append("\n");
		for (int j = SIZE - 1; j >= 0; j--) {
			sb.append(j);
			for (int i = 0; i < SIZE; i++) {
				sb.append(state[i][j].getSymbol());
				if (i < SIZE - 1) {
					sb.append(" ");
				}
			}
			sb.append(j);
			sb.append("\n");
		}
		sb.append(" ");
		for (int i = 0; i < SIZE; i++) {
			sb.append(i);
			sb.append(" ");
		}
		return sb.toString();
	}
	
	
}
