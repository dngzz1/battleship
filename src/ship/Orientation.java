package ship;

import java.util.Random;

public enum Orientation {
	HORIZONTAL, VERTICAL;
	
	public static Orientation getRandom() {
		Random random = new Random();
		return Orientation.values()[random.nextInt(Orientation.values().length)];
	}
}