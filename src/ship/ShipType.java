package ship;

import grid.State;

public enum ShipType {
	CARRIER(5, "5"),
	BATTLESHIP(4, "4"),
	CRUISER(3, "3"),
	SUBMARINE(3, "3"),
	DESTROYER(2, "2");
	
	private final int length;
	private final String symbol;
	ShipType(int length, String symbol) {
		this.length = length;
		this.symbol = symbol;
	}
	
	public int getLength() {
		return length;
	}
	public String getSymbol() {return symbol; }

	public State getState() {
		switch(this){
			case CARRIER:
				return State.CARRIER;
			case BATTLESHIP:
				return State.BATTLESHIP;
			case CRUISER:
				return State.CRUISER;
			case SUBMARINE:
				return State.SUBMARINE;
			case DESTROYER:
			default:
				return State.DESTROYER;
		}
	}
}
