package ship;

/*
 * Ship contains data: 
 * Orientation: whether it is horizontal or vertical.
 * Length: can be any integer at least 1.
 * (x,y): the head of the position and then extending rightwards or upwards.
 */
public class Ship {
	
	final Orientation orientation;
	final ShipType shipType;
	final int length;
	final int x;
	final int y;
	
	public Ship(Orientation orientation, ShipType shipType, int x, int y) {
		this.orientation = orientation;
		this.shipType = shipType;
		this.length = shipType.getLength();
		this.x = x;
		this.y = y;
	}
	
	public Orientation getOrientation( ) {
		return orientation;
	}
	
	public int getLength() {
		return length;
	}
	
	public int getX() {
		return x;
	}
	
	public int getY() {
		return y;
	}

	public ShipType getShipType() {return shipType; }
}
